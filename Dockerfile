# image de base python a partir du docker hub
FROM python:3.10-alpine

# work directory
WORKDIR /app

# set variables d'environnement 
ENV PYTHONDONTWRITEBYTECODE 1 
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0
# PYTHONDONTWRITEBYTECODE empeche l'ecriture des fichiers pycache

# installer mysql
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add --no-cache mariadb-dev \
    && pip install mysqlclient \
    && apk add --no-cache bash

# installer dependances 
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# copier projet
COPY . .

# RUN mkdir /app/static

# collect static files
RUN python manage.py collectstatic --noinput    


# ajouter et run un non-root user 
RUN adduser -D myuser
USER myuser

# CMD gunicorn barkolog.wsgi:application --bind 0.0.0.0:$PORT-





