

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db import IntegrityError
from .forms import UserRegisterForm,  BreedCreateForm,DogCreateForm

from .models import Breed, Dog,Owner, Walk

# ------------------Authentification----------------------------

# LOGIN
def my_login(request):
    if request.method == 'POST':
        email = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'connexion reussi')
            user = Owner.objects.get(pk=request.user.id)
            dog = Dog.objects.filter(owner=user)
            if dog :
                return redirect('index')
            else:
                messages.success(request, ('veuillez ajouter un chien pour proceder plus loin'))
                return redirect ('add_dog')
        else:
            messages.success(request, ("L'utilisateur ou le mot de passe n'existe pas "))
            return redirect('login')
    else:
        return render(request, 'barko/login.html')    

#  REGISTER
def inscription(request):
    user_form = UserRegisterForm()
    if request.method == 'POST':
        user_form = UserRegisterForm(request.POST or None)
        if user_form.is_valid():
            try:
                last_name = request.POST['last_name']
                firstname = request.POST['first_name']
                pwd = request.POST['password1']
                email = request.POST['email']
                username = email
                user = User.objects.create_user(username, email, pwd)    
                user.last_name = last_name
                user.first_name = firstname
                user.save()                
                login(request,user)
                messages.success(request, ('inscripton reussi'))
                messages.success(request, ('veuillez ajouter un chien pour proceder plus loin'))
                return redirect ('add_dog')                
            except IntegrityError : 
                messages.error(request, ('Un compte éxiste déja '))
                user_form = UserRegisterForm()
                context = {'user_form': user_form } 
                return render(request, 'barko/inscription.html',context) 
        else:
            messages.error(request, ('Veuillez corriger le formulaire.'))
    else:
        user_form = UserRegisterForm()
    context = {'user_form': user_form } 
    return render(request, 'barko/inscription.html',context) 

# LOGOUT
def my_logout(request):
    logout(request)
    messages.success(request, ('déconnexion reussi'))
    return redirect('index')



# --------------------------APP----------------------------

# HOME
@login_required(login_url='login')   
def index(request):
    user = request.user.owner
    dog = Dog.objects.filter(owner=user)
    walk = Walk.objects.all
    context={'dog_list':dog, 'user':user, 'walk':walk}
    return render(request, 'barko/index.html', context)

# DOG ACTIVITIES VIEW
@login_required(login_url='login')   
def journal(request):
    user = request.user.owner
    dog = Dog.objects.filter(owner=user)
    return render(request, 'barko/journal.html', {'dog_list': dog, 'user':user})

# ---------------------------DOG CRUD-----------------------

# CREATE DOG
@login_required(login_url='login')  
def add_dog(request):
    dog_form = DogCreateForm()  
    owner = Owner.objects.get(owner=request.user)     
    if request.method == 'POST': 
        dog_form = DogCreateForm(request.POST or None,  request.FILES or None)
        if dog_form.is_valid():
            new_dog = dog_form.save()
            owner.dog.add(new_dog)
            owner.save()
            messages.success(request, ('Chien enrengistré'))
            return redirect('index')
        else:
            dog_form = DogCreateForm()
            messages.error(request, ('verifier les informations'))
    else:
        dog_form = DogCreateForm()
    return render(request,'dog/add_dog.html', {'dog_form': dog_form})


#  READ DOG LIST
@login_required(login_url='login')   
def dog_profil(request):
    user = request.user.owner
    dog = Dog.objects.filter(owner=user )
    return render(request, 'dog/dog_profil.html', {'dogs': dog, 'user':user})


# UPDATE SINGLE DOG 
@login_required(login_url='login')   
def single_dog_profile(request, dog_id): 
    dog = Dog.objects.get(id=dog_id)  
    form = DogCreateForm(request.POST or None, request.FILES or None, instance=dog)
    if form.is_valid():
        form.save()
        messages.success(request, 'informations mise a jour')
        return redirect('dog_profil')
    context={'dog': dog, 'form':form}
    return  render(request, 'dog/single_dog.html', context) 

# DELETE SINGLE DOG
@login_required(login_url='login')   
def delete_dog(request, dog_id):
    dog = Dog.objects.get(id=dog_id)
    dog.delete()
    messages.success(request, ('Le chien a été supprimer'))
    return redirect('dog_profil')


# *-----------------------------USER ------------------------

# READ USER
@login_required(login_url='login')   
def right(request):
    current_user = request.user
    user= User.objects.get(user=current_user)
    return render(request, 'base/right.html', {'user':user})


# ----------------------------ADMIN-------------------------
@login_required(login_url='login')  
def add_breed(request):
    breed_form = BreedCreateForm()
    if request.method == 'POST':    
        breed_form = BreedCreateForm(request.POST or None)
        if breed_form.is_valid():
            breed_form.save()
            messages.success(request, ('la race a bien été ajouter'))
            return redirect('add_dog')
    else:
        breed_form = BreedCreateForm()
    return render(request, 'dog/add_breed.html', {'breeds': breed_form})



























# ---------------------------------FUTUR--------------------------

@login_required(login_url='login')   
def parametres(request):    
    context={}
    return render(request, 'barko/parametres.html', context)

# def ajouter(request):
#     return render(request, 'barko/ajouter.html')

# def maps(request):
#     return render(request, 'barko/maps.html')

# def budget(request):
#     return render(request, 'barko/budget.html')

# def objectif(request):
#     return render(request, 'barko/objectif.html')
