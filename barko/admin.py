from django.contrib import admin
from .models import Owner, Dog, Breed, Vaccine, Feeding, FoodBrand, FoodType, Photo, Walk



admin.site.register(Owner)
admin.site.register(Breed)
admin.site.register(Vaccine)
admin.site.register(Feeding)
admin.site.register(FoodBrand)
admin.site.register(FoodType)
admin.site.register(Photo)
admin.site.register(Walk)




@admin.register(Dog)
class DogAdmin(admin.ModelAdmin):
    list_display =  ( 'name_dog','breed_dog', )
