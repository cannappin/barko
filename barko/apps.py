from django.apps import AppConfig


class BarkoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'barko'
