from django.urls import path
from . import views 

urlpatterns = [
    path('', views.index, name='index'),
    path('journal', views.journal, name='journal'),
    path('parametres', views.parametres, name='parametres'),    
    
    path('inscription', views.inscription, name='inscription'),
    path('login', views.my_login, name='login'),
    path('logout', views.my_logout, name='logout'),

    path('dog_profil', views.dog_profil, name='dog_profil'), 
    path('single_dog/<int:dog_id>/', views.single_dog_profile, name='single_dog'), 
    path('add_dog', views.add_dog, name='add_dog'),
    path('add_breed', views.add_breed, name='add_breed'),
    path('delete_dog/<int:dog_id>', views.delete_dog, name='delete_dog'),

]



    # -------------FUTUR---------------------------
    # path('ajouter', views.ajouter, name='ajouter'),
    # path('maps', views.maps, name='maps'),
    # path('budget', views.budget, name='budget'),
    # path('objectif', views.objectif, name='objectif'),
    