from django.db import models
import datetime
from django.contrib.auth.models import User
from django.utils.timezone import now
from django.db.models.signals import post_save
from django.dispatch import receiver

        

    
class Breed(models.Model):
    name_breed = models.CharField(max_length=30)    
    
    def __str__(self):
        return self.name_breed    
    
class FoodType(models.Model):
    name_food_type = models.CharField(max_length=30, blank=True, null=True)    
    
    def __str__(self):
        return self.name_food_type

class FoodBrand(models.Model):
    name_brand = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self):
        return self.name_brand
    
class Photo(models.Model):
    description_photo = models.CharField(max_length=255, blank=True, null=True)
    path_photo = models.ImageField(upload_to='dogs', null=True)
    date_photo = models.DateTimeField('date de publication de photo',default=datetime.datetime.now)
    
    def __str__(self):
        return self.description_photo
        

class Dog(models.Model):
    FEMALE = 'Femelle'
    MALE = 'Mâle'
    SEXE_CHOICES = [(FEMALE,'femelle'),(MALE,'mâle')]
    name_dog = models.CharField(max_length=30)
    avatar_dog = models.ImageField(upload_to='dog/dog_avatar', default='/default/8.png', editable=True)
    age_month_dog = models.DateField("date de naissance", default=now, editable=True)
    sex_dog = models.CharField('sexe du chien', choices=SEXE_CHOICES, blank=True, null=True, max_length=20, default=MALE)
    weight_dog = models.FloatField('poids du chien',default=0, null=True, blank=True)
    id_chip_dog = models.BigIntegerField(blank=True, null=True)
    sterilized = models.BooleanField('stériliser ou pas', null=True)
    breed_dog = models.ForeignKey(Breed, on_delete=models.CASCADE, verbose_name='race du chien' ,blank=True, null=True)
    photo = models.ManyToManyField(Photo, blank=True)

    def __str__(self):
        return self.name_dog 


class Owner(models.Model):
    owner = models.OneToOneField(User, on_delete=models.CASCADE)
    age_owner = models.DateField("date de naissance", default=now, editable=True)
    avatar = models.ImageField(upload_to = 'user', default='default/avatar.jpg', editable=True)
    dog = models.ManyToManyField(Dog)

    @receiver(post_save, sender=User)
    def create_owner_profile(sender, instance, created, **kwargs):
        if created:
            Owner.objects.create(owner=instance)

    @receiver(post_save, sender=User)
    def save_owner_profile(sender, instance, **kwargs):
        instance.owner.save()

    def __str__(self):
        return self.owner.first_name


class Feeding(models.Model):
    quantity = models.IntegerField('Quantité en grams',default=0)
    begin_time = models.DateTimeField("début d'un repas", default=now, editable=True)
    end_time = models.DateTimeField(" fin du repas ", default=now, editable=True)
    comments = models.CharField(max_length=255, blank=True, null=True)
    type = models.ForeignKey(FoodType, on_delete=models.CASCADE, blank=True, null=True)
    brand = models.ForeignKey(FoodBrand, on_delete=models.CASCADE, blank=True)
    photo = models.ManyToManyField(Photo, blank=True)   
    dog = models.ManyToManyField(Dog)

    def __str__(self):
        return self.type.name_food_type
        
class Walk(models.Model):
    name = models.CharField(max_length=30, default='quotidienne', editable=True)
    begin_time= models.DateTimeField('date/heure de debut', default=now, editable=True)
    end_time = models.DateTimeField('date/heure de fin', default=now, editable=True)
    comments = models.CharField(max_length=200, blank=True, null=True)
    photo_walk = models.ImageField(upload_to='walks', blank= True)
    dog = models.ManyToManyField(Dog)
    
    
    def __str__(self):
        return self.name

class Vaccine(models.Model):
    name_vacinne = models.CharField(max_length=30)
    date_vaccine = models.DateField('', blank=True, null=True)
    vaccinated = models.BooleanField('yes or no', blank=True, null=True)
    dog = models.ForeignKey(Dog, on_delete=models.CASCADE, blank=True, null=True)   
    
    def __str__(self):
        return self.name_vacinne
    
    
