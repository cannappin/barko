



from django.forms import ModelForm 
from .models import Breed, Dog, Feeding, Owner, Photo
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm



class UserRegisterForm(UserCreationForm):  
    class Meta:
        model = User
        fields = ( 'first_name', 'last_name', 'email', 'password1', 'password2')
        first_name = forms.CharField(required=True)
        widgets = {
            'first_name':forms.TextInput(attrs={'class': 'form-control', 'required':True}),
            'last_name':forms.TextInput(attrs={'class': 'form-control', 'required':True}),
            'email':forms.EmailInput(attrs={'class': 'form-control', 'required':True}),
            'password1':forms.PasswordInput(attrs={'class': 'form-control'}),
            'password2':forms.PasswordInput(attrs={'class': 'form-control'}),
        } 
        labels = {
            'first_name':'Prénom',
            'last_name':'Nom',
            'email':'Adress mail',
            'password1':'Mot de passe',
            'password2':'Confirmer mot de passe ',
        } 
    
    def __init__(self,*args,**kwargs):        
        """ passe 'class':'form-control' au champs password1 et password2 
        -> BOOTSTRAP RENDERING
        """
        super(UserRegisterForm,self).__init__(*args,**kwargs)
        self.fields['password1'].widget.attrs['class']= 'form-control'
        self.fields['password2'].widget.attrs['class']= 'form-control'
        self.fields['password1'].label = 'Mot de passe'
        self.fields['password2'].label = 'Confirmer mot de passe'







class DogCreateForm(ModelForm):

    class Meta :
        model = Dog
        fields = ('name_dog', 'avatar_dog',  'age_month_dog','sex_dog', 'weight_dog','id_chip_dog','sterilized','breed_dog')
        breed_dog =forms.ChoiceField()
        
        
        labels ={
            'name_dog':'Nom du chien',
            'avatar_dog':'Photo du chien',
            'age_month_dog':'Date de naissance',
            'sex_dog':'le sexe du chien',
            'weight_dog':'Poids',
            'id_chip_dog':'Puce d\'intification',
            'sterilized':'Steriliser',
            'breed_dog':'',      
        }
        widgets ={
            'name_dog':forms.TextInput(attrs={'class': 'form-control mt-2 mb-4'}),
            'avatar_dog':forms.ClearableFileInput(attrs={'class': 'form-control mt-2 mb-4'}),
            'age_month_dog':forms.DateInput(attrs={'class': 'form-control mt-2 mb-4', 'type':'date'}),
            'sex_dog':forms.Select(attrs={'class': 'form-select mt-2 mb-4'}),
            'weight_dog':forms.NumberInput(attrs={'min':'0', 'type': 'number','class':'form-control mt-2 mb-4'}),
            'id_chip_dog':forms.NumberInput(attrs={'min':'100000000000000', 'max':'999999999999999', 'placeholder':'code unique de 15 chiffres ',
                                                    'class':'form-control mt-2 mb-4'}),
            'sterilized':forms.CheckboxInput(attrs={'class':'form-check-input mt-2 mb-4 col-12'}),
            'breed_dog':forms.Select(attrs={'class': 'form-select mt-2 mb-4', 'placeholder':'race du chien'}),  
        }

class BreedCreateForm(ModelForm):

    class Meta:
        model = Breed
        fields = ('name_breed',)
        name_breed = forms.CharField()
        labels ={
            'name_breed':'Nom de la race'
        }
        widgets = {
            'name_breed':forms.TextInput(attrs={'class': 'form-control'}),
        }




# class OwnerRegisterForm(ModelForm):    
#     class Meta:
#         model = Owner
#         fields =('name', 'age_owner', 'avatar')       
#         widgets ={
#             'age_owner':forms.DateInput(attrs={'class':'form-control'}),
#             'avatar':forms.ClearableFileInput(attrs={'class': 'form-control'}),
#             'name':forms.TextInput(attrs={'class': 'form-control'})
#         } 